﻿-- zlicza ile jest odgromówek na powerliniach
SELECT
  powerlines.name AS Powerlines_name,
  count(powerlines.name) AS Ground_cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE cables.type=1
GROUP BY powerlines.name
ORDER BY powerlines.name


-- pokazuje na których liniach są odgromówki i jakie mają nazwy
SELECT
  powerlines.name as Powerlines_name, 
  cables.name as Ground_cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE cables.type=1
ORDER BY powerlines.name, cables.name

-- pokazuje czy na danej linii są odgromówki oraz jakie
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  powerlines.name AS Powerlines_name, 
  cables.name AS Ground_cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
ORDER BY powerlines.name, cables.name
