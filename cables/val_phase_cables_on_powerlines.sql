﻿-- zlicza ile jest dodanych kabli na powerliniach 
SELECT
  powerlines.name AS Powerlines_name,
  COUNT(powerlines.name) AS All_Cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE cables.type<>1
GROUP BY powerlines.name
ORDER BY powerlines.name


-- pokazuje jakie cable są dodane do powerlinii i jakie mają nazwy
SELECT
  powerlines.name AS Powerlines_name,
  cables.name AS All_Cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE cables.type<>1
ORDER BY powerlines.name, cables.name


-- pokazuje jakie cable są dodane do danej powerlinii i jakie mają nazwy
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  powerlines.name AS Powerlines_name,
  cables.name AS All_Cables
FROM 
  powerlines
INNER JOIN cables ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')  AND cables.type<>1
ORDER BY powerlines.name, cables.name



