﻿-- usuwa wszystkie odgromówki z linii
DELETE FROM polesincable WHERE id_cable IN (SELECT ID from cables WHERE TYPE=1);


--deleting ground cables from powerline with cables from DB (second part)
SET SESSION my.powerline_name = 'KIL'; 
DELETE FROM polesincable WHERE id_cable IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%'));
DELETE FROM cables WHERE id IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%'));

--deleting ground cables from powerline with cables from DB (second part) - determined cable name and powerline
SET SESSION my.powerline_name = 'KIL'; 
DELETE FROM polesincable WHERE id_cable IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') AND cables.name='G2');

DELETE FROM cables WHERE id IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') AND cables.name='G2');



--deleting ground cables from powerline list
DELETE FROM polesincable WHERE id_cable IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name IN ('L11","L12'));
DELETE FROM cables WHERE id IN (SELECT
cables.ID
FROM 
  cables
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE cables.type=1 and powerlines.name IN ('L11',"L12'))