-- pokazuje wszystkie poligony na bazie
SELECT
  polygonpoints.id,
  polygonpoints.pif_of_laser,
  polygonpoints.height,
  polygonpoints.usr,
  polygonpoints.add_time
FROM
  polygonpoints
ORDER BY polygonpoints.id

-- zlicza poligony na bazie
SELECT
  count(polygonpoints.id) as PolygonSum
FROM
  polygonpoints

-- zlicza poligony na bazie według użytkownika
SELECT
  count(polygonpoints.id) as PolygonSum,
  SUBSTRING(polygonpoints.usr,1,char_length(polygonpoints.usr)) as str
FROM
  polygonpoints
GROUP BY str
ORDER BY str