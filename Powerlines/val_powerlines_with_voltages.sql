SELECT
  powerlines.id, 
  powerlines.name,
  powerlines.description,
  voltage.voltage,
  voltage.voltage_number
FROM 
  powerlines
INNER JOIN voltage on powerlines.id_voltage=voltage.id
ORDER BY powerlines.name