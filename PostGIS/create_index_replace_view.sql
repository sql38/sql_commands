
--create id index
ALTER table point ADD COLUMN id serial primary key;
ALTER table line ADD COLUMN id serial primary key;
ALTER table polygon ADD COLUMN id serial primary key;
 
 --
create or replace view materialized view testview as (
 SELECT line.id,
    line.name,
    line.way::geometry
   FROM line
  WHERE line.highway IS NOT NULL
UNION ALL
 SELECT polygon.id,
    polygon.name,
    polygon.way::geometry
   FROM polygon
  WHERE polygon.highway IS NOT NULL
UNION ALL
 SELECT point.id,
    point.name,
    point.way::geometry
   FROM point
  WHERE point.highway IS NOT NULL);