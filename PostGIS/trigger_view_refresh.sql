CREATE OR REPLACE FUNCTION refreshtestview3()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW testview3;
    RETURN NULL;
END;
$$;

create trigger refresh_testview4 after INSERT OR UPDATE OR DELETE ON 
test FOR EACH STATEMENT EXECUTE PROCEDURE refreshtestview3();