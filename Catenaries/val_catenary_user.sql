--  zlicza userów catenar na bazie
SELECT 
  COUNT(catenaries.id),
  catenaries.usr
FROM 
  catenaries
GROUP BY catenaries.usr

--  zlicza userów catenar na powerliniach
SELECT 
  COUNT(catenaries.id),
  catenaries.usr,
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY catenaries.usr,powerlines.name
ORDER BY catenaries.usr,powerlines.name

-- pokazuje userów catenar na powerlini
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  catenaries.id,
  catenaries.usr,
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY catenaries.id, catenaries.usr,powerlines.name
ORDER BY catenaries.usr,powerlines.name