SELECT
   cat_ID,
   ROUND(SQRT((P2_X - P1_X) ^ 2 + (P2_Y - P1_Y) ^ 2)::NUMERIC, 2) AS span_length 
FROM
   (
      SELECT
         c.id AS cat_ID,
         pic1.id_pole AS id_p1,
         (
            SELECT
               ground_point[0] 
            FROM
               poles 
            WHERE
               id = pic1.id_pole
         )
         AS P1_X,
         (
            SELECT
               ground_point[1] 
            FROM
               poles 
            WHERE
               id = pic1.id_pole
         )
         AS P1_Y,
         pic2.id_pole AS id_p2,
         (
            SELECT
               ground_point[0] 
            FROM
               poles 
            WHERE
               id = pic2.id_pole
         )
         AS P2_X,
         (
            SELECT
               ground_point[1] 
            FROM
               poles 
            WHERE
               id = pic2.id_pole
         )
         AS P2_Y 
      FROM
         catenaries c,
         polesincable pic1,
         polesincable pic2 
      WHERE
         c.id_pole1 = pic1.id_polincab 
         AND c.id_pole2 = pic2.id_polincab 
         AND pic1.id_cable = pic2.id_cable
   )
   AS innerquery