--clean all temperatures
update catenaries set
temp_thermal=NULL,
temp_catenary=NULL,
temp_ambient=NULL,
emissivity=NULL,
temp_date=NULL,
temp_meas_type=1

--clean temperatures by date
update catenaries set
temp_thermal=NULL,
temp_catenary=NULL,
temp_ambient=NULL,
emissivity=NULL,
temp_date=NULL,
temp_meas_type=1
where temp_date<'2019-05-30'

-- clean temperature on powerline
update catenaries set
temp_thermal=NULL,
temp_catenary=NULL,
temp_ambient=NULL,
emissivity=NULL,
temp_date=NULL,
temp_meas_type=1
where catenaries.id in (SELECT c.id 
   FROM catenaries c, polesincable pic, cables cab, powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND pow.name like '%NAME%')
