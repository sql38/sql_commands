--  zlicza catenary bez client_info na bazie
SELECT 
  COUNT(catenaries.id),
  'Empty_client_info' as Description_catenaries
FROM 
  catenaries
WHERE catenaries.client_info IS NULL 


--  zlicza catenary bez client_info na powerliniach
SELECT 
  COUNT(catenaries.id),
  powerlines.name,
  catenaries.client_info
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE catenaries.client_info IS NULL 
GROUP BY catenaries.client_info,powerlines.name
ORDER BY powerlines.name

-- pokazuje catenary bez client_info  na danej powerlini
SET SESSION my.powerline_name = 'L1S2'; --tu wpisz nazwe linii
SELECT 
  catenaries.id,
  powerlines.name,
  catenaries.client_info
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE catenaries.client_info IS NULL and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY catenaries.id, catenaries.client_info,powerlines.name
ORDER BY powerlines.name