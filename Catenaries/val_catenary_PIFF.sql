--  liczy ile razy jaki piff catenary wystepuje dla wszystkich linii w bazie
SELECT 
  COUNT(catenaries.id) AS COUNT,
  catenaries.pif_of_laser,
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name IN (SELECT powerlines.name FROM powerlines) 
GROUP BY catenaries.pif_of_laser, powerlines.name
ORDER BY powerlines.name, COUNT;
 
-- sprawdzenie jakie piffy catenar sa na danej linii i jak często wystepują (wpisz nazwe linii)
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  COUNT(catenaries.id),
  catenaries.pif_of_laser
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY catenaries.pif_of_laser
ORDER BY catenaries.pif_of_laser;

-- odnajduje ID_catenar, których piff najrzadziej występuje 
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  catenaries.id, 
  catenaries.pif_of_laser,
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE catenaries.pif_of_laser=(SELECT 
	  catenaries.pif_of_laser
	FROM 
	  catenaries
          INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
          INNER JOIN cables ON polesincable.id_cable=cables.id
          INNER JOIN powerlines ON cables.id_powerline=powerlines.id
	WHERE 
	  powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
	  GROUP BY catenaries.pif_of_laser ORDER BY COUNT(catenaries.pif_of_laser) LIMIT 1)
AND powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
ORDER BY catenaries.id
