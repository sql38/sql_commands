--  zlicza catenary bez dodanych materiałów w bazie
SELECT 
  COUNT(catenaries.id),
  'Empty materials' as Description_catenaries
FROM 
  catenaries
WHERE catenaries.id_mat IS NULL 

--  zlicza catenary bez dodanych materiałów na powerliniach
SELECT 
  COUNT(catenaries.id),
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE catenaries.id_mat IS NULL 
GROUP BY catenaries.id_mat,powerlines.name
ORDER BY powerlines.name

-- pokazuje catenary bez dodanych materiałów na danej powerlini
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  catenaries.id,
  powerlines.name
FROM 
  catenaries
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE catenaries.id_mat IS NULL and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY catenaries.id, catenaries.id_mat,powerlines.name
ORDER BY powerlines.name