--function checking if insert/update tipologia values is in matching array from dict table (matched by table name)
CREATE OR REPLACE FUNCTION tipologia_check() RETURNS TRIGGER AS $$
DECLARE
	col text := 'tipologia';
BEGIN
    IF NEW is NULL or 
	(SELECT NEW.tipologia = ANY (allowed_values::varchar[]) FROM _dict WHERE table_name=TG_TABLE_NAME AND column_name='tipologia')=False 
	
	THEN
        RAISE exception 'Invalid tipologia value: % ',NEW.tipologia;
	ELSE 
	RETURN NEW;
    END IF;
   
END
$$ language plpgsql;

-- CREATE TRIGGER IN LOOP
CREATE OR REPLACE PROCEDURE trigger_creator()
as $$
	DECLARE
	  r RECORD;
	BEGIN
	  FOR r IN (SELECT *
			   FROM information_schema.tables
			   where 
			   		table_type='BASE TABLE' AND 
					 table_schema = 'public' AND
					table_name not in ('spatial_ref_sys', '_dict'))
	  LOOP
		RAISE NOTICE 'CREATE TRIGGER FOR: %', r.table_name::text;

		EXECUTE 'CREATE OR REPLACE TRIGGER tipologia_trigger
		  BEFORE INSERT OR UPDATE ON
		  ' || r.table_name || ' FOR EACH ROW
		EXECUTE PROCEDURE tipologia_check();';
	  END LOOP;
	END;
$$ LANGUAGE plpgsql;
CALL trigger_creator();