--objects on DB
((select '1' as LP,'Powerlines' as DESCRIPTION_____, count(*) from powerlines)
union
(select '2' as LP, 'Catenaries' as DESCRIPTION_____, count(*) from catenaries)
union
(select '3' as LP, 'Poles' as DESCRIPTION_____, count(*) from poles)
union
(select '4' as LP, 'Collisions' as DESCRIPTION_____, count(*) from collisions where note like '')
union
(select '5' as LP,'DT' as DESCRIPTION_____, count(*) from treeattributes)
union
(select '6' as LP,'Water_polygons' as DESCRIPTION_____, count(*) from polygonpoints))
order by LP


--modification time
((select '1' as LP, 'Min Time poles' As DESCRIPTION_OF_TIME____,min(poles.mod_time), 'Max Time poles' As DESCRIPTION_OF_TIME___ , max(poles.mod_time) from poles)
union
(select '2' as LP, 'Min Time catenaries' As DESCRIPTION_OF_TIME____, min(catenaries.mod_time), 'Max Time catenaries' As DESCRIPTION_OF_TIME___,max(catenaries.mod_time) from catenaries)
union
(select '3' as LP, 'Min Time collisions' As DESCRIPTION_OF_TIME____, min(collisions.mod_time), 'Max Time collisions' As DESCRIPTION_OF_TIME___,max(collisions.mod_time) from collisions where note like '')
union
(select '4' as LP,  'Min Time collisions' As DESCRIPTION_OF_TIME____, min(collisions.mod_time), 'Max Time Trees' As DESCRIPTION_OF_TIME___,max(collisions.mod_time) from collisions where not note like ''))
ORDER BY LP
