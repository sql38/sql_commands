    ALTER TABLE poles ADD COLUMN id_pole_material integer;   
   
    CREATE TABLE pole_materials (
id serial NOT NULL,
name character varying(100) NOT NULL,
    CONSTRAINT pole_materials_pkey PRIMARY KEY (id),
    CONSTRAINT name_unique UNIQUE (name)
    );
   
    ALTER TABLE ONLY poles
ADD CONSTRAINT poles_id_pole_materials_fkey FOREIGN KEY (id_pole_material) REFERENCES pole_materials(id);
   
    CREATE OR REPLACE VIEW v_polesnames AS
    SELECT btrim(COALESCE(pn.name, p.name)::text) AS name_diff, v.id_pole,
v.id_powerline, v.powerline, v.series_name AS series_name,
pn.id AS id_pn, pn.name AS name_pn,
p.id,
p.pif_of_laser, p.laser_start, p.laser_end, p.top, p.point3d,
p.ground_point, p.ground_point3d, p.planar1_point, p.planar1_point3d,
p.planar2_point, p.planar2_point3d, p.base_point, p.base_point3d,
p.second_ground_point, p.second_ground_point3d, p.recalculations, p.name,
p.id_pole_type, p.id_construction, p.sides, p.id_ref_system,
p.base_rotation, p.base_figure, p.add_time, p.mod_time, p.usr,
p.phase_number, p.ground_dist_manual, p.french_construction_material,
p.french_construction, p.isolators_number, p.pole_plate, p.pole_state,
p.support_segment_point, p.note, p.inclination_points
    FROM v_polesinpowerline v
    LEFT JOIN polesnames pn ON v.id_pole = pn.id_pole AND v.id_powerline = pn.id_powerline,
poles p
    WHERE p.id = v.id_pole
    ORDER BY v.id_powerline;\