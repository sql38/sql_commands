-- zlicza i pokazuje ilu użytkowników wstawiało słupy na bazie
SELECT
  COUNT(poles.usr),
  poles.usr
FROM poles
GROUP BY poles.usr
ORDER BY poles.usr

-- zlicza i pokazuje ilu użytkowników wstawiało słupy na wszystkich liniach w bazie
SELECT
  COUNT(poles.usr),
  poles.usr,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY poles.usr,powerlines.name
ORDER BY powerlines.name,poles.usr

-- zlicza i pokazuje ilu użytkowników wstawiało słupy na l powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  COUNT(poles.usr),
  poles.usr,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.usr,powerlines.name
ORDER BY powerlines.name,poles.usr

--pokazuje na jakich powerliniach wstawiał słupy dany użytkownik
SET SESSION my.usr = '.szczepkowski'; --tu wpisz nazwe użytkownika
SELECT
  COUNT(poles.usr),
  poles.usr,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.usr LIKE CONCAT('%',current_setting('my.usr')::VARCHAR,'%')
GROUP BY poles.usr,powerlines.name
ORDER BY powerlines.name,poles.usr

--pokazuje kiedy dany użytkownik wstawił słupy na danej powerlinii
SET SESSION my.usr = '.szczepkowski'; --tu wpisz nazwe użytkownika
SET SESSION my.powerline_name = 'M'; --tu wpisz nazwe linii
SELECT
 poles.id,
  poles.usr,
  powerlines.name,
  poles.add_time,
  poles.mod_time
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.usr LIKE CONCAT('%',current_setting('my.usr')::VARCHAR,'%') and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.id, powerlines.name
ORDER BY powerlines.name,poles.id