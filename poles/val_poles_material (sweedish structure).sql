-- pokazuje ile słupów na bazie ma puste materiały słupów
SELECT
  count(poles.id),
  'No_material' as Description
FROM poles
LEFT JOIN pole_materials ON poles.id_pole_material=pole_materials.id
WHERE poles.id_pole_material IS NULL
UNION
SELECT
  count(poles.id),
  'All_poles' as Description
FROM poles

-- zlicza puste nazwy materiałów na wszystkich powerliniach
SELECT
  count(poles.id),
  pole_materials.name,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
LEFT JOIN pole_materials ON poles.id_pole_material=pole_materials.id
WHERE poles.id_pole_material IS NULL
GROUP BY powerlines.name, pole_materials.name
ORDER BY powerlines.name

--pokazuje wszystkie puste nazwy materiałów na danej linii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  count(poles.id),
  pole_materials.name,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
LEFT JOIN pole_materials ON poles.id_pole_material=pole_materials.id
WHERE poles.id_pole_material IS NULL and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY powerlines.name, pole_materials.name
ORDER BY powerlines.name

