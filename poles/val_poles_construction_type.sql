-- zlicza ile słupów nie ma dodanych konstrukcji na bazie
SELECT
  COUNT(poles.id),
  constructions.construction
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
WHERE constructions.construction='unknown'
GROUP BY constructions.construction
ORDER BY constructions.construction

--zlicza ile słupów nie ma dodanych konstrukcji na powerliniach
SELECT
  COUNT(poles.id),
  constructions.construction,
  powerlines.name
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE constructions.construction='unknown'
GROUP BY powerlines.name,constructions.construction 
ORDER BY powerlines.name,constructions.construction

--pokazuje które słupy nie mają konstrukcji na danej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  constructions.construction,
  powerlines.name
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') AND constructions.construction='unknown'
GROUP BY poles.id, powerlines.name,constructions.construction
ORDER BY poles.id,constructions.construction



--STATYSTYKA
--zlicza wszystkie typy konstrukcji na bazie
SELECT
  COUNT(poles.id),
  constructions.construction
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
GROUP BY constructions.construction
ORDER BY constructions.construction

--zlicza wszystkie typy konstrukcji na powerliniach
SELECT
  COUNT(poles.id),
  constructions.construction,
  powerlines.name
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY powerlines.name,constructions.construction 
ORDER BY powerlines.name,constructions.construction

--zlicza wszystkie typy konstrukcji na danej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  COUNT(poles.id),
  constructions.construction,
  powerlines.name
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY powerlines.name,constructions.construction
ORDER BY powerlines.name,constructions.construction