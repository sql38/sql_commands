-- pokazuje jaka powerlinia jest przypisana do słupa po id_słupa
SELECT
  poles.id,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id=123
GROUP BY powerlines.name,poles.id

-- pokazuje jaka powerlinia jest przypisana do słupa po id_słupów z listy
SELECT
  poles.id,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id IN (846, 1635, 1642)
GROUP BY powerlines.name,poles.id
