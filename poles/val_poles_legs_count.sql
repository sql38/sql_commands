-- pokazuje ile słupów w bazie nie ma dodanej liczby nóg
SELECT
  COUNT(poles.id),
    'No_legs' as Description
FROM poles 
WHERE poles.id NOT IN (SELECT 
                             poles.id 
                          FROM poles 
                          INNER JOIN poleinfo ON poles.id=poleinfo.pole_id 
                          WHERE poleinfo.polelegcount IS NOT NULL)
UNION
SELECT
  count(poles.id),
  'All_poles' as Description
FROM poles

--zlicza ile słupów nie ma dodanej liczby nóg na powerliniach
SELECT
  COUNT(poles.id),
  powerlines.name
FROM poles 
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id NOT IN (SELECT 
                             poles.id 
                          FROM poles 
                          INNER JOIN poleinfo ON poles.id=poleinfo.pole_id 
                          WHERE poleinfo.polelegcount IS NOT NULL)
GROUP BY powerlines.name
ORDER BY powerlines.name

--pokazuje jakie słupy nie mają dodanej liczby nóg na powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  powerlines.name
FROM poles 
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id NOT IN (SELECT 
                             poles.id 
                          FROM poles 
                          INNER JOIN poleinfo ON poles.id=poleinfo.pole_id 
                          WHERE poleinfo.polelegcount IS NOT NULL)
AND powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.id, powerlines.name
ORDER BY poles.id, powerlines.name


