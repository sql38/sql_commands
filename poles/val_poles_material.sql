-- pokazuje ile słupów na bazie ma puste materiały 
SELECT
  count(poles.id),
  'No_material' as Description
FROM poles
WHERE poles.french_construction_material=0
UNION
SELECT
  count(poles.id),
  'All_poles' as Description
FROM poles

-- zlicza puste nazwy materiałów na wszystkich powerliniach
SELECT
  count(poles.id),
  poles.french_construction_material,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.french_construction_material=0
GROUP BY powerlines.name, poles.french_construction_material
ORDER BY powerlines.name

--pokazuje wszystkie puste nazwy materiałów słupów na bazie
SELECT
  poles.id,
  poles.french_construction_material,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.french_construction_material=0
GROUP BY poles.id, powerlines.name, poles.french_construction_material
ORDER BY poles.id,powerlines.name

--pokazuje wszystkie puste nazwy materiałów słupów na danej linii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  poles.french_construction_material,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.french_construction_material=0 and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.id, powerlines.name, poles.french_construction_material
ORDER BY poles.id,powerlines.name