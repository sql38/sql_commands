-- zlicza puste id_clienta słupów na bazie 
SELECT
  COUNT(poles.id),
  'WITH_CLIENT_ID' as Description
FROM poles
WHERE poles.id_client IS NULL OR poles.id_client=0
UNION
SELECT
  count(poles.id),
  'All_poles' as Description
FROM poles

-- zlicza puste id_clienta słupów na wszystkich powerliniach
SELECT
  COUNT(poles.id),
  poles.id_client,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id_client IS NULL OR poles.id_client=0
GROUP BY powerlines.name, poles.id_client
ORDER BY powerlines.name

--pokazuje wszystkie puste id_clienta słupów na bazie
SELECT
  poles.id,
  poles.id_client,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.id_client IS NULL OR poles.id_client=0
GROUP BY poles.id, poles.id_client,powerlines.name
ORDER BY powerlines.name,poles.id

--pokazuje puste id_clienta słupów na danej linii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  poles.id_client,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') and (poles.id_client IS NULL OR poles.id_client=0)
GROUP BY poles.id, poles.id_client, powerlines.name
ORDER BY poles.id