--zlicza wszystkie tensiony/suspensiony na bazie
SELECT
  COUNT(poles.id),
  CASE
    WHEN poles.id_pole_type=1 THEN 'TENSION'
    WHEN poles.id_pole_type=0 THEN 'SUSPENSION'
  END AS ___PolesType
FROM poles
GROUP BY poles.id_pole_type 
ORDER BY poles.id_pole_type 


--zlicza wszystkie tensiony/suspensiony na powerliniach
SELECT
  COUNT(poles.id),
  CASE
    WHEN poles.id_pole_type=1 THEN 'TENSION'
    WHEN poles.id_pole_type=0 THEN 'SUSPENSION'
  END AS ___PolesType,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY powerlines.name,poles.id_pole_type 
ORDER BY powerlines.name,poles.id_pole_type 

--zlicza wszystkie tensiony/suspensiony na danej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  COUNT(poles.id),
  CASE
    WHEN poles.id_pole_type=1 THEN 'TENSION'
    WHEN poles.id_pole_type=0 THEN 'SUSPENSION'
  END AS ___PolesType,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY powerlines.name,poles.id_pole_type 
ORDER BY powerlines.name,poles.id_pole_type 