﻿-- pokazuje ile słupów na bazie ma puste nazwy w porównaniu do całej bazy
SELECT
  count(poles.name),
  'No name' as Description
FROM poles
WHERE poles.name=''
UNION
SELECT count(poles.id), 'All_poles' FROM poles 


-- zlicza puste nazwy słupów na wszystkich powerliniach
SELECT
  count(poles.name),
  poles.name,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.name=''
GROUP BY powerlines.name, poles.name
ORDER BY powerlines.name

--pokazuje wszystkie puste nazwy słupów na bazie
SELECT
  poles.id,
  poles.name,
  powerlines.name
FROM poles
INNER JOIN constructions ON poles.id_construction=constructions.id
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.name=''
GROUP BY poles.id, poles.name, powerlines.name
ORDER BY powerlines.name,poles.id

--pokazuje puste nazwy słupów na danej linii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  poles.name,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.name='' and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.id, poles.name, powerlines.name
ORDER BY poles.id