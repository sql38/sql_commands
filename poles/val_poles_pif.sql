-- zlicza i pokazuje ile danych piffów słupów jest na liniach w bazie
SELECT
  COUNT(poles.pif_of_laser),
  poles.pif_of_laser,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY poles.pif_of_laser, powerlines.name
ORDER BY powerlines.name

-- zlicza i pokazuje ile danych piffów słupów jest na danej linii w bazie
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  COUNT(poles.pif_of_laser),
  poles.pif_of_laser,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.pif_of_laser, powerlines.name
ORDER BY powerlines.name

--odnajduje ID slupow na linii, których PIFF najrzadziej występuje
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT
  poles.id,
  poles.pif_of_laser,
  powerlines.name
FROM poles
INNER JOIN polesincable ON poles.id=polesincable.id_pole
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE poles.pif_of_laser=(SELECT poles.pif_of_laser
						  FROM poles
						  INNER JOIN polesincable ON poles.id=polesincable.id_pole
						  INNER JOIN cables ON polesincable.id_cable=cables.id
						  INNER JOIN powerlines ON cables.id_powerline=powerlines.id
						  WHERE powerlines.name LIKE '%ML316%'
						  GROUP BY poles.pif_of_laser ORDER BY COUNT(poles.pif_of_laser) LIMIT 1)
and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY poles.id,poles.pif_of_laser,powerlines.name
ORDER BY poles.id,poles.pif_of_laser

