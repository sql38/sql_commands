 --check note (excluded area on powerlines)
 SELECT pow.name, col.note
   FROM collisions col, catenaries c, polesincable pic, cables cab, 
    powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND note <>'' GROUP BY pow.name, col.note ORDER BY pow.name


 --check unique note in tree job name
 SELECT col.note
   FROM collisions col, catenaries c, polesincable pic, cables cab, 
    powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND note <>'' GROUP BY col.note