﻿--sprawdza liczbę jobów oraz liczbę powerlinii na bazie
SELECT 
   COUNT(collisionjob.id),
   'Job_number' AS Description
FROM
collisionjob
UNION
SELECT
   COUNT(powerlines.id),
   'Powerlines_number' AS Description
FROM
powerlines

--szuka zdublowanych jobów
SELECT SUBSTRING(collisionjob.name,1,strpos(collisionjob.name,'_tree')) AS Powerline, COUNT(*)
FROM collisionjob GROUP BY Powerline 
HAVING COUNT(*) > 1
ORDER BY Powerline

