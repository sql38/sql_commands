SET SESSION my.powerline_name = '130KV_NÄSSJÖ-EKSJÖ-MARIANNELUND'; --tu wpisz nazwe linii
UPDATE treeattributes SET species=2 WHERE id_col IN 
(SELECT t.id_col 
FROM
	collisions col, 
	catenaries c, 
	polesincable pic, 
	cables cab, 
	treeattributes t,
	powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND 
  t.id_col = col.id AND pow.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%'))
  
  
  
--bez like
SET SESSION my.powerline_name = '50KV_TORSEBRO-TILL_AVG_OSBY_AVGR_KNISLINGE'; --tu wpisz nazwe linii
UPDATE treeattributes SET species=2 WHERE id_col IN 
(SELECT t.id_col 
FROM
	collisions col, 
	catenaries c, 
	polesincable pic, 
	cables cab, 
	treeattributes t,
	powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND 
  t.id_col = col.id AND pow.name LIKE current_setting('my.powerline_name')::VARCHAR)