-- sprawdza na jakich liniach są dodane manualnie drzewa z lasera (CTRL+U)
SELECT
  count(collisions.note),
  collisions.note,
  powerlines.name
FROM 
  collisions
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE collisions.note LIKE 'manual measurement'
GROUP BY powerlines.name, collisions.note 
ORDER BY powerlines.name