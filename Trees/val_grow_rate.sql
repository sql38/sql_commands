-- zlicza dla ilu drzew na jakich liniach nie obliczono nowej wysokości (jest taka sama w dwóch latach)
SELECT 
  powerlines.name,
  COUNT(treeattributes.id),
 CASE
   WHEN treeattributes.grow_est_method=-1 THEN 'NOT CALCULATE'
   WHEN treeattributes.grow_est_method=1 THEN 'DIFFERENTIAL'
   WHEN treeattributes.grow_est_method=2 THEN 'STATISTICAL - DH NEGATIVE'
   WHEN treeattributes.grow_est_method=3 THEN 'STATISTICAL'
 END AS Grow_est_method,
  treeattributes.grow_rate,
  treeattributes.grow_rate_curve,
  treeattributes.height_year1,
  treeattributes.height_year2
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE treeattributes.height_year1=treeattributes.height_year2 
GROUP BY powerlines.name, treeattributes.height_year1,treeattributes.height_year2,treeattributes.grow_rate,treeattributes.grow_rate_curve,treeattributes.grow_est_method
ORDER BY powerlines.name



-- zlicza dla ilu drzew na jakich liniach growth rate=0
SELECT 
  COUNT(collisions.id),
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE treeattributes.grow_rate=0 AND treeattributes.grow_rate_curve=0
GROUP BY powerlines.name
ORDER BY powerlines.name


-- sprawdza dla jakich drzew growth rate=0 lub height_year1=height_year2
SELECT 
  collisions.id,
  treeattributes.height_year1,
  treeattributes.height_year2,
 CASE
   WHEN treeattributes.grow_est_method=-1 THEN 'NOT CALCULATE'
   WHEN treeattributes.grow_est_method=1 THEN 'DIFFERENTIAL'
   WHEN treeattributes.grow_est_method=2 THEN 'STATISTICAL - DH NEGATIVE'
   WHEN treeattributes.grow_est_method=3 THEN 'STATISTICAL'
 END AS Grow_est_method,
  treeattributes.grow_rate,
  treeattributes.grow_rate_curve,
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE (treeattributes.grow_rate=0 AND treeattributes.grow_rate_curve=0) OR (treeattributes.height_year1=treeattributes.height_year2)
ORDER BY powerlines.name