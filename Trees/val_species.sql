﻿-- sprawdza czy puszczono klasyfikację drzew na wszystkich powerliniach w bazie (czy nie ma unknown w species)
SELECT 
  powerlines.name,
  COUNT(treeattributes.id)
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE species=1
GROUP BY powerlines.name
ORDER BY powerlines.name


-- sprawdza ile gatunków drzew (species) występuje po automatycznej klasyfikacji na liniach w bazie
SELECT 
  powerlines.name,
  COUNT(treeattributes.id),
CASE
  WHEN treeattributes.species=1 THEN 'Unknown'
  WHEN treeattributes.species=2 THEN 'Pine'
  WHEN treeattributes.species=3 THEN 'Spruce'
  WHEN treeattributes.species=4 THEN 'Birch'
END as Species
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY species,powerlines.name
ORDER BY powerlines.name


-- sprawdza ile gatunków drzew występuje po automatycznej klasyfikacji na  danej linii w bazie
SET SESSION my.powerline_name = '316';
SELECT 
  COUNT(treeattributes.id),
CASE
  WHEN treeattributes.species=1 THEN 'Unknown'
  WHEN treeattributes.species=2 THEN 'Pine'
  WHEN treeattributes.species=3 THEN 'Spruce'
  WHEN treeattributes.species=4 THEN 'Birch'
END as Species
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY species
