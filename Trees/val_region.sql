-- pokazuje jakie regiony sa na bazie, zlicza drzewa na bazie
SELECT 
  COUNT(powerlines.name),
  powerlines.name,
CASE
  WHEN treeattributes.region=0 THEN 'NORR'
  WHEN treeattributes.region=1 THEN 'SOUTH'
  WHEN treeattributes.region=2 THEN 'MIDDLE' 
END AS Region
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY powerlines.name,treeattributes.region
ORDER BY treeattributes.region, powerlines.name


-- pokazuje jakie regiony sa na wszystkich liniach w bazie
SELECT 
  COUNT(treeattributes.id),
CASE
  WHEN treeattributes.region=0 THEN 'NORR'
  WHEN treeattributes.region=1 THEN 'SOUTH'
  WHEN treeattributes.region=2 THEN 'MIDDLE' 
END AS Region
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY treeattributes.region
ORDER BY treeattributes.region



-- pokazuje jakie regiony sa na danej linii 
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  COUNT(treeattributes.id),
CASE
  WHEN treeattributes.region=0 THEN 'NORR'
  WHEN treeattributes.region=1 THEN 'SOUTH'
  WHEN treeattributes.region=2 THEN 'MIDDLE' 
END AS Region
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY treeattributes.region

