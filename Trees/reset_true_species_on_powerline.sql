--reset true species on powerline

SET SESSION my.powerline_name = 'PSE-4-LES2-POL2-0'; --tu wpisz nazwe linii
UPDATE treeattributes SET species_true=1 where treeattributes.id_col in (
	SELECT col.id
		FROM collisions col, catenaries c, polesincable pic, cables cab, treeattributes t,powerlines pow
	WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id 
	AND pow.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%'))

