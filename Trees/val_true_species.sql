﻿-- sprawdza ile drzew z ilu na calej powerlinii sklasyfikowano manualnie na wszystkich powerliniach w bazie
(SELECT 
  COUNT(treeattributes.id) AS classify,
  powerlines.name,
  'Manual' AS Method
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE species_true!=1
GROUP BY powerlines.name
ORDER BY powerlines.name)
UNION ALL
(SELECT 
  count(treeattributes.species) as all_trees,
  powerlines.name,
  'Automatic' AS Method
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY powerlines.name
ORDER BY powerlines.name)
ORDER BY name, method DESC


-- sprawdza po ile gatunków drzew sklasyfikowano na powerliniach w bazie
SELECT 
  count(treeattributes.id),
CASE
  WHEN treeattributes.species_true=1 THEN 'Unknown'
  WHEN treeattributes.species_true=2 THEN 'Pine'
  WHEN treeattributes.species_true=3 THEN 'Spruce'
  WHEN treeattributes.species_true=4 THEN 'Birch'
END AS True_Species,
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY species_true,powerlines.name
ORDER BY powerlines.name,species_true


-- sprawdza po ile gatunków drzew sklasyfikowano na danej powerlinii 
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  COUNT(treeattributes.id),
CASE
  WHEN treeattributes.species_true=1 THEN 'Unknown'
  WHEN treeattributes.species_true=2 THEN 'Pine'
  WHEN treeattributes.species_true=3 THEN 'Spruce'
  WHEN treeattributes.species_true=4 THEN 'Birch'
END as True_Species
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY species_true
