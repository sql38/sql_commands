--DANGEROUS TREES ON DB
SELECT COUNT(*) from collisions, treeattributes WHERE collisions.id=treeattributes.id_col AND collisions.isok=True


-- DANGEROUS TREES TO CLASSIFY
SELECT count(col.id), pow.name
   FROM collisions col, catenaries c, polesincable pic, cables cab, treeattributes t,
    powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id and 
  t.id_col = col.id AND col.isok=True AND t.species=1
  
  GROUP BY pow.name ORDER BY pow.name
