-- count trees on powerlines
SELECT count(col.id), pow.name
   FROM collisions col, catenaries c, polesincable pic, cables cab, treeattributes t,
    powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id and 
  t.id_col = col.id group by pow.name order by pow.name