﻿-- sprawdza nazwy joba drzew dla wszystkich powerlinii w bazie oraz czy zgadza sie nazwa powerliniii z nazwą joba (brak nazwy powerlinii oznacza brak drzew na linii)
SELECT 
   collisionjob.id,
   powerlines.name,
   collisionjob.name,
   collisionjob.usr,
   Case
   WHEN strpos(collisionjob.name,powerlines.name)>0 THEN 'OK'
   WHEN strpos(collisionjob.name,powerlines.name)=0 THEN 'MISTAKE'
   END AS Validation
FROM 
  powerlines
RIGHT JOIN cables ON cables.id_powerline=powerlines.id
RIGHT JOIN polesincable ON polesincable.id_cable=cables.id
RIGHT JOIN catenaries ON catenaries.id_pole1=polesincable.id_polincab
RIGHT JOIN collisions ON collisions.id_cat=catenaries.id
RIGHT JOIN collisionjob ON collisions.id_collision_job=collisionjob.id
GROUP BY collisionjob.name, powerlines.name,collisionjob.id
ORDER BY Validation

-- sprawdza nazwy joba drzew dla wszystkich powerlinii w bazie oraz kto go zapuścił
SELECT 
   powerlines.name,
   collisionjob.name, 
   collisionjob.usr
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
INNER JOIN collisionjob ON collisions.id_collision_job=collisionjob.id
GROUP BY collisionjob.name, powerlines.name,collisionjob.usr
ORDER BY powerlines.name