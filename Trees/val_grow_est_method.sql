-- liczy ile drzew obliczono jaką metodą na powerliniach w bazie
SELECT 
  COUNT(treeattributes.id_col), 
 CASE
   WHEN treeattributes.grow_est_method=-1 THEN 'NOT CALCULATE'
   WHEN treeattributes.grow_est_method=1 THEN 'DIFFERENTIAL'
   WHEN treeattributes.grow_est_method=2 THEN 'STATISTICAL - DH NEGATIVE'
   WHEN treeattributes.grow_est_method=3 THEN 'STATISTICAL'
 END as Grow_est_method,
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY powerlines.name, treeattributes.grow_est_method
ORDER BY treeattributes.grow_est_method, powerlines.name


-- liczy ile drzew obliczono jaką metodą na danej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  COUNT(treeattributes.id_col),
 CASE
   WHEN treeattributes.grow_est_method=-1 THEN 'NOT CALCULATE'
   WHEN treeattributes.grow_est_method=1 THEN 'DIFFERENTIAL'
   WHEN treeattributes.grow_est_method=2 THEN 'STATISTICAL - DH NEGATIVE'
   WHEN treeattributes.grow_est_method=3 THEN 'STATISTICAL'
 END as Grow_est_method,
 powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY treeattributes.grow_est_method,powerlines.name
ORDER BY treeattributes.grow_est_method


-- pokazuje informacje o grow_est_method dla drzew na danej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  treeattributes.id_col, 
 CASE
   WHEN treeattributes.grow_est_method=-1 THEN 'NOT CALCULATE'
   WHEN treeattributes.grow_est_method=1 THEN 'DIFFERENTIAL'
   WHEN treeattributes.grow_est_method=2 THEN 'STATISTICAL - DH NEGATIVE'
   WHEN treeattributes.grow_est_method=3 THEN 'STATISTICAL'
 END as Grow_est_method,
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
ORDER BY treeattributes.grow_est_method
