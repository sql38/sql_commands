﻿-- zlicza i pokazuje jaką metodą zapuszczono dzewa TREE_U -> all trees, TREE_C -> CurrentDT, TREE_F -> Future DT, IG => excluded area
SELECT 
  COUNT(note),
  collisions.note,
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY collisions.note, powerlines.name
ORDER BY collisions.note

-- sprawdza czy zapuszczono drzewa z metodą wg wzoru
SET SESSION my.condition = 'TREE_C;TT=C;IG=5.0'; --tu wpisz co ma być sprawdzane w note
SELECT 
  COUNT(note),
  collisions.note,
  powerlines.name,
  CASE
  WHEN STRPOS(collisions.note,current_setting('my.condition')::VARCHAR)>0 THEN 'OK'
  WHEN STRPOS(collisions.note,current_setting('my.condition')::VARCHAR)=0 THEN 'MISTAKE'
  END AS Validation
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
GROUP BY collisions.note, powerlines.name
ORDER BY Validation