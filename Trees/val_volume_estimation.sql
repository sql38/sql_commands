﻿-- sprawdza dla ilu drzew na jakich liniach nie obliczono volume (nic nie zwróci - wszystko ok)
SELECT 
  COUNT(treeattributes.id),
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE  treeattributes.diameter_trunk=0 OR treeattributes.volume=0
GROUP BY powerlines.name
ORDER BY powerlines.name

-- sprawdza dla ilu drzew na jakich liniach nie obliczono grup wegetacji (nic nie zwróci - wszystko ok)
SELECT 
  COUNT(treeattributes.id),
  powerlines.name
FROM 
  treeattributes
INNER JOIN collisions ON treeattributes.id_col = collisions.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE  treeattributes.grow_group=-1
GROUP BY powerlines.name
ORDER BY powerlines.name


