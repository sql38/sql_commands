-- zasetowanie wszystkich kolizji na false (drzew i kolizji)
UPDATE collisions SET isok='f' WHERE isok='t'

-- zasetowanie kolizji, ktore nie są drzewami na TRUE
UPDATE collisions SET isok='t' WHERE note=''


-- zasetowanie wybranych kolizji po ID_DATEBASE na TRUE (trzeba mieć listę id_drzew)
UPDATE collisions SET isok='t' WHERE id IN (
1,
2
);

-- usunięcie drzew z atrubutem FALSE
DELETE FROM collisions where isok='f'



TRUNCATE treeattributes CASCADE;
TRUNCATE collisions CASCADE;
TRUNCATE collisionjob CASCADE;