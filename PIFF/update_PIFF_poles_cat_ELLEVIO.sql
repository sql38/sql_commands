SET SESSION my.powerline_name = 'L114_5';
SET SESSION my.PIFF = '6244496453526';

update catenaries set pif_of_laser=current_setting('my.PIFF')::bigint where id in(
 SELECT c.id AS id_cat
   FROM catenaries c, polesincable pic, cables cab, powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id and pow.name=current_setting('my.powerline_name')::VARCHAR);

update poles set pif_of_laser=current_setting('my.PIFF')::bigint where id in (
SELECT p.id 
   FROM poles p, polesincable pic, cables cab, 
    powerlines pow
  WHERE p.id = pic.id_pole AND cab.id_powerline = pow.id AND pic.id_cable = cab.id and pow.name=current_setting('my.powerline_name')::VARCHAR
  GROUP BY p.id, pow.id, pow.name);