--show size of all database on server
SELECT pg_database.datname as "database_name", pg_database_size(pg_database.datname)/1024/1024 AS size_in_mb FROM pg_database ORDER by size_in_mb DESC;


-- Database Size
SELECT pg_size_pretty(pg_database_size('Database Name'));