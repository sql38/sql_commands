--pokazuje kolizje, które nie spełniają warunku (po przeliczeniu R-ką)
SELECT 
   collisions.id,
   maxtempjob.name,
   ROUND(collisions.distance::NUMERIC,2) as Distance
FROM  
   collisions
INNER JOIN collisionsfortemp ON collisions.id_colfortemp=collisionsfortemp.id
INNER JOIN maxtempjob ON collisionsfortemp.id_maxtempjob=maxtempjob.id
WHERE maxtempjob.name like '%vegetation_4%' AND ROUND(collisions.distance::NUMERIC,2)<4
ORDER BY Distance


--pokazuje daną kolizję i jej joba
SELECT 
   maxtempjob.id,
   collisions.distance,
   coldistsettings.name
FROM  
   collisions
INNER JOIN collisionsfortemp ON collisions.id_colfortemp=collisionsfortemp.id
INNER JOIN maxtempjob ON collisionsfortemp.id_maxtempjob=maxtempjob.id
WHERE collisions.id=54534


-- pokazuje ile jest kolizji w danym warunku na danej powerlinii
SELECT count(sett.name),sett.name,pow.name
   FROM collisions col, catenaries c, polesincable pic, cables cab, collisionsfortemp tempp, maxtempjob job,coldistsettings sett,
    powerlines pow
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND col.id_colfortemp=tempp.id AND 
 tempp.id_maxtempjob=job.id AND tempp.id_coldistsettings=sett.id GROUP BY pow.name, sett.name ORDER BY pow.name, sett.name

