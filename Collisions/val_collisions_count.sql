--zlicza kolizje na powerliniach w bazie
SELECT 
  powerlines.name,
  COUNT(collisions.id)
FROM 
  collisions
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE collisions.note=''
GROUP BY powerlines.name
ORDER BY powerlines.name

--zlicza kolizje na wybranej powerlinii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
  powerlines.name,
  COUNT(collisions.id)
FROM 
  collisions
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE collisions.note='' AND powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%')
GROUP BY powerlines.name

--zlicza kolizje na powerliniach w bazie z podziałem na warunki
SELECT 
   powerlines.name,
   COUNT(collisions.id)   
   coldistsettings.name


FROM  
   collisions
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
INNER JOIN voltage ON powerlines.id_voltage=voltage.id
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
WHERE collisions.note=''
GROUP BY powerlines.name
ORDER BY powerlines.name
