-- delete collisions with distance, voltage of powerline
delete from collisions where collisions.id in(
SELECT col.id 
   FROM collisions col, catenaries c, polesincable pic, cables cab, 
    powerlines pow, voltage v
  WHERE c.id_pole1 = pic.id_polincab AND pic.id_cable = cab.id AND cab.id_powerline = pow.id AND col.id_cat = c.id AND pow.id_voltage=v.id and v.id=8 and col.distance>3 and col.note='')