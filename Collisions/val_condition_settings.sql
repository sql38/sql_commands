--T5
SELECT
  coldistsettings.name,coldistsettings.ptclass,coldistsettings.type,
  CASE
    WHEN coldistsettings.cabletype=-1 THEN 'ALL_CABLES'
	WHEN coldistsettings.cabletype=0 THEN 'PHASE_CABLES'
	WHEN coldistsettings.cabletype=1 THEN 'GROUND_CABLES'
	END AS CABLE_TYPE,
  CASE
    WHEN classdist.iso_type=1 THEN 'UNKNOWN'
    WHEN classdist.iso_type=2 THEN 'COVERED'
    WHEN classdist.iso_type=3 THEN 'UNCOVERED'
  END AS Isolation_type,
  voltage.voltage,
  collisionfilters.test_angle,collisionfilters.small_radius,collisionfilters.ignore_objects,
  classdist.dist
FROM
coldistsettings
INNER JOIN collisionfilters ON coldistsettings.id=collisionfilters.id_coldistsettings
INNER JOIN classdist ON coldistsettings.id=classdist.id_coldistsettings
INNER JOIN voltage ON classdist.id_voltage=voltage.id
ORDER BY coldistsettings.name

--BOX
SELECT
  coldistsettings.name,coldistsettings.ptclass,
  CASE 
    WHEN coldistsettings.type=0 THEN 'Centerline'
	WHEN coldistsettings.type=0 THEN 'Centerline_ground'
	WHEN coldistsettings.type=0 THEN 'Dynamic'
	WHEN coldistsettings.type=0 THEN 'Dynamic_ground'
	END AS Collision_type,
  CASE
    WHEN coldistsettings.cabletype=-1 THEN 'ALL_CABLES'
	WHEN coldistsettings.cabletype=0 THEN 'PHASE_CABLES'
	WHEN coldistsettings.cabletype=1 THEN 'GROUND_CABLES'
	END AS CABLE_TYPE,
  voltage.voltage,
  classdist.dist,classdist.dist2,classdist.dist3,classdist.dist4,
  CASE
    WHEN classdist.iso_type=1 THEN 'UNKNOWN'
    WHEN classdist.iso_type=2 THEN 'COVERED'
    WHEN classdist.iso_type=3 THEN 'UNCOVERED'
  END AS Isolation_type,
  classdist.anchor1,classdist.anchor2,classdist.anchor3, classdist.anchor4,
  classdist.angle1,classdist.angle2,classdist.angle3,classdist.angle4
FROM
coldistsettings
INNER JOIN classdist ON coldistsettings.id=classdist.id_coldistsettings
INNER JOIN voltage ON classdist.id_voltage=voltage.id
ORDER BY coldistsettings.name