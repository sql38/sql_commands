﻿-- sprawdza nazwe joba i warunków dla wszystkich linii w bazie, sprawdza czy nazwa joba to nazwa warunku (Vattenfall)
SELECT 
   maxtempjob.id,
   powerlines.name, 
   voltage.voltage,
SUBSTRING(maxtempjob.name,19,19) AS MAXTEMPJOB_NAME,
SUBSTRING(coldistsettings.name,1,19) AS CONDITION_NAME,
   Case
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,19),SUBSTRING(coldistsettings.name,1,19))>0 THEN 'OK'
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,19),SUBSTRING(coldistsettings.name,1,19))=0 THEN 'MISTAKE'
   END AS Validation
FROM  
   powerlines
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
INNER JOIN voltage ON powerlines.id_voltage=voltage.id
ORDER BY validation, maxtempjob.id


-- sprawdza nazwe joba i warunków wybranych dla wybranej linii (wpisz nazwę linii)
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
   maxtempjob.id,
   powerlines.name, 
   maxtempjob.name,
   coldistsettings.name,
   Case
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,18),SUBSTRING(coldistsettings.name,1,18))>0 THEN 'OK'
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,18),SUBSTRING(coldistsettings.name,1,18))=0 THEN 'MISTAKE'
   END AS Validation
FROM  
   powerlines
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
WHERE powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
   
   
-- sprawdza nazwe joba, nazwy powerlinii i warunków dla wybranego napiecia
SET SESSION my.voltage = '130'; --tu wpisz napięcie
SELECT 
   maxtempjob.id,
   powerlines.name, 
   voltage.voltage,
   maxtempjob.name,
   coldistsettings.name,
   Case
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,18),SUBSTRING(coldistsettings.name,1,18))>0 THEN 'OK'
   WHEN STRPOS(SUBSTRING(maxtempjob.name,19,18),SUBSTRING(coldistsettings.name,1,18))=0 THEN 'MISTAKE'
   END AS Validation
FROM  
   powerlines
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
INNER JOIN voltage ON powerlines.id_voltage=voltage.id
WHERE voltage.voltage LIKE CONCAT('%',current_setting('my.voltage')::VARCHAR,'%') 
ORDER BY powerlines.name, maxtempjob.name