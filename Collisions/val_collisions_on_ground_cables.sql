SELECT 
c.id AS catenary_id

FROM collisions col

LEFT JOIN catenaries c ON col.id_cat = c.id
JOIN polesincable pic ON c.id_pole1 = pic.id_polincab
JOIN cables cab ON pic.id_cable = cab.id
WHERE cab.type=1 AND col.note=''
GROUP BY c.id
  