SELECT maxtempjob.name,
    powerlines.name,
    maxtempjob.usr,
    coldistsettings.name
FROM maxtempjob,
     collisionsfortemp,
     coldistsettings,
     powerlines
WHERE maxtempjob.id=collisionsfortemp.id_maxtempjob
  AND collisionsfortemp.id_coldistsettings=coldistsettings.id
  AND maxtempjob.id_powerline=powerlines.id
  AND coldistsettings.id=3
ORDER BY powerlines.name