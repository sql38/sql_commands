--szuka zdublowanych jobów (te same warunki na tych samych liniach)
SELECT 
   powerlines.name, 
   coldistsettings.name,
   COUNT(*)
FROM  
   powerlines
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
GROUP BY coldistsettings.name, powerlines.name
HAVING COUNT(*)>1
ORDER BY powerlines.name


--pokazuje ile jest zapuszczonych warunków na wszystkich powerliniach
SELECT 
   COUNT(maxtempjob.id),
   powerlines.name
FROM  
   powerlines
LEFT JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
LEFT JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
LEFT JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
GROUP BY powerlines.name
ORDER BY COUNT(maxtempjob.id)


--pokazuje nazwy powerlinii o liczbie jobów różnej od np. 3
SELECT 
   COUNT(maxtempjob.id),
   powerlines.name
FROM  
   powerlines
LEFT JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
LEFT JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
LEFT JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
GROUP BY powerlines.name
HAVING COUNT(maxtempjob.id)<>3
ORDER BY  powerlines.name