-- display count unique collision conditions on powerlines
SELECT 
   powerlines.name, 
   count(DISTINCT maxtempjob.name)
FROM  
   powerlines
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
GROUP BY powerlines.name
ORDER BY powerlines.name
