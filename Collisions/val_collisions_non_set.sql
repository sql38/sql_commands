-- sprawdza ile jest niezasetowanych kolizji maxtemp wg powerlinii i użytkownika, który zapuścił joba (na całej bazie)
SELECT 
count(maxtempjob.usr),
powerlines.name, 
maxtempjob.usr
FROM  
   collisions
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
WHERE collisions.isok='f' and collisions.id_colfortemp IS NOT NULL
GROUP BY maxtempjob.usr, powerlines.name
ORDER BY maxtempjob.usr



-- sprawdza ile jest niezasetowanych kolizji maxtemp na danej linii
SET SESSION my.powerline_name = '316'; --tu wpisz nazwe linii
SELECT 
count(maxtempjob.usr),
powerlines.name, 
maxtempjob.usr
FROM  
   collisions
INNER JOIN catenaries ON collisions.id_cat=catenaries.id
INNER JOIN polesincable ON catenaries.id_pole1=polesincable.id_polincab
INNER JOIN cables ON polesincable.id_cable=cables.id
INNER JOIN powerlines ON cables.id_powerline=powerlines.id
INNER JOIN maxtempjob ON powerlines.id=maxtempjob.id_powerline
INNER JOIN collisionsfortemp ON maxtempjob.id=collisionsfortemp.id_maxtempjob
INNER JOIN coldistsettings ON collisionsfortemp.id_coldistsettings=coldistsettings.id
WHERE collisions.isok='f' and collisions.id_colfortemp IS NOT NULL and powerlines.name LIKE CONCAT('%',current_setting('my.powerline_name')::VARCHAR,'%') 
GROUP BY maxtempjob.usr, powerlines.name
ORDER BY maxtempjob.usr